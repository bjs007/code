var express = require('express');
var port = process.env.PORT||8080;
var app = express();
var path = require('path');
var bodyParser = require('body-parser');




app.use(express.static(__dirname+'/public'));
app.use(express.static(__dirname+'/css'));
app.set('views',path.join(__dirname,'/views'));

var routes = require("./app/routes.js")(app);
app.listen(port);
console.log('Magic happens on port'+port);
